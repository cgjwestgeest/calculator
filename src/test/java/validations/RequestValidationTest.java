package validations;

import calculator.model.OperandDTO;
import calculator.model.OperatorDTO;
import calculator.validations.InvalidRequestException;
import calculator.validations.RequestValidation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes=RequestValidation.class)
public class RequestValidationTest {

    @Autowired
    private
    RequestValidation requestValidation;

    @Test
    public void shouldThrowNoExceptionOperator() throws InvalidRequestException {
        requestValidation.validateOperatorDTO(new OperatorDTO("ADD"));
    }

    @Test
    public void shouldThrowNoExceptionOperand() throws InvalidRequestException {
        requestValidation.validateOperandDTO(new OperandDTO(400));
    }

    @Test(expected = InvalidRequestException.class)
    public void shouldThrowExceptionWrongOperator() throws InvalidRequestException {
        requestValidation.validateOperatorDTO(new OperatorDTO("Cavia"));
    }

    @Test(expected = InvalidRequestException.class)
    public void shouldThrowExceptioEmptyOperator() throws InvalidRequestException {
        requestValidation.validateOperatorDTO(null);
    }

    @Test(expected = InvalidRequestException.class)
    public void shouldThrowExceptioEmptyOperand() throws InvalidRequestException {
        requestValidation.validateOperandDTO(null);
    }
}
