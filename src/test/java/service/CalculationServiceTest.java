package service;

import calculator.model.CalculationDTO;
import calculator.model.OperandDTO;
import calculator.model.OperatorDTO;
import calculator.service.CalculationService;
import calculator.validations.InvalidRequestException;
import calculator.validations.RequestValidation;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes= {CalculationService.class, RequestValidation.class})
public class CalculationServiceTest {

    @Autowired
    CalculationService calculationService;

    @Test
    public void shouldAddFirstOperand() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(500), calculationDTO);
        assertEquals(500, calculationDTO.getFirstOperand(), 0);
    }

    @Test
    public void shouldSetOperator() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.setOperator(new OperatorDTO("ADD"), calculationDTO);
        assertEquals("ADD", calculationDTO.getOperator());
    }

    @Test(expected = InvalidRequestException.class)
    public void shouldThrowInvalidRequestException() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.setOperator(new OperatorDTO("Hamster"), calculationDTO);
    }

    @Test
    public void shouldSetOperatorAndCalculateRoot() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(4), calculationDTO);
        assertEquals(2, calculationService.setOperator(new OperatorDTO("ROOTOF"), calculationDTO), 0);
    }

    @Test
    public void shouldAddUp() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(4), calculationDTO);
        calculationService.setOperator(new OperatorDTO("ADD"), calculationDTO);
        assertEquals(10, calculationService.addOperand(new OperandDTO(6), calculationDTO), 0);
    }

    @Test
    public void shouldSubtract() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(10), calculationDTO);
        calculationService.setOperator(new OperatorDTO("SUBTRACT"), calculationDTO);
        assertEquals(4, calculationService.addOperand(new OperandDTO(6), calculationDTO), 0);
    }

    @Test
    public void shouldMultiply() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(5), calculationDTO);
        calculationService.setOperator(new OperatorDTO("MULTIPLY"), calculationDTO);
        assertEquals(25, calculationService.addOperand(new OperandDTO(5), calculationDTO), 0);
    }

    @Test
    public void shouldDivide() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(10), calculationDTO);
        calculationService.setOperator(new OperatorDTO("DIVIDE"), calculationDTO);
        assertEquals(5, calculationService.addOperand(new OperandDTO(2), calculationDTO), 0);
    }

    @Test
    public void shouldDivideAndReturnInfinity() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(10), calculationDTO);
        calculationService.setOperator(new OperatorDTO("DIVIDE"), calculationDTO);
        assertTrue(Double.isInfinite(calculationService.addOperand(new OperandDTO(0), calculationDTO)));
    }

    @Test
    public void shouldDivideAndReturnNaN() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(0), calculationDTO);
        calculationService.setOperator(new OperatorDTO("DIVIDE"), calculationDTO);
        assertTrue(Double.isNaN(calculationService.addOperand(new OperandDTO(0), calculationDTO)));
    }

    @Test
    public void shouldCalculatePower() throws InvalidRequestException {
        CalculationDTO calculationDTO = new CalculationDTO();
        calculationService.addOperand(new OperandDTO(10), calculationDTO);
        calculationService.setOperator(new OperatorDTO("POWEROF"), calculationDTO);
        assertEquals(100, calculationService.addOperand(new OperandDTO(2), calculationDTO), 0);
    }

}