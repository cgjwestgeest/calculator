package calculator.validations;

import calculator.model.OperandDTO;
import calculator.model.OperatorDTO;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Service
public class RequestValidation {

    private final Set<String> allowedValues = new HashSet<>(Arrays.asList("ADD", "SUBTRACT", "DIVIDE", "MULTIPLY", "ROOTOF", "POWEROF"));

    public void validateOperatorDTO(OperatorDTO operatorDTO) throws InvalidRequestException {
        if (operatorDTO==null) {
            throw new InvalidRequestException("No input provided");
        }
        if (!allowedValues.contains(operatorDTO.getOperator())) {
            throw new InvalidRequestException("Invalid operator. Allowed inputs are: ADD, SUBTRACT, DIVIDE, MULTIPLY, ROOTOF, POWEROF");
        }
    }

    public void validateOperandDTO(OperandDTO operandDTO) throws InvalidRequestException {
        if (operandDTO==null) {
            throw new InvalidRequestException("No input provided");
        }
    }
}
