package calculator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class OperandDTO {

    @Getter @Setter
    private double operand = 0;
}
