package calculator.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Component
@Scope("session")
@NoArgsConstructor
public class CalculationDTO implements Serializable {
    @Getter @Setter
    private double firstOperand;
    @Getter @Setter
    private double secondOperand;
    @Getter @Setter
    private String operator;
}
