package calculator.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class OperatorDTO {

    @Getter @Setter
    public String operator;
}
