package calculator.service;

import calculator.model.CalculationDTO;
import calculator.model.OperandDTO;
import calculator.model.OperatorDTO;
import calculator.validations.InvalidRequestException;
import calculator.validations.RequestValidation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class CalculationService {

    @Autowired
    private RequestValidation requestValidation;

    /**
     * Adds an operand to the calculation DTO. If this no operator is set yet, input operand is added as the first operand
     * and the value of this operand is returned.
     * If an operator is present, the input operand is set as the second operand and an attempt at calculation is made.
     * The output of the calculation is returned.
     * @param operandDTO DTO containing the operand value
     * @param calculationDTO DTO containing the calculation values
     * @return output to be presented to the user as a double
     */
    public double addOperand(OperandDTO operandDTO, CalculationDTO calculationDTO) throws InvalidRequestException {
        // validate input
        requestValidation.validateOperandDTO(operandDTO);
        // check if an operator is present
        if (calculationDTO.getOperator()==null) {
            // if not, set the first operand
            log.debug("First operand set to "+operandDTO.getOperand());
            calculationDTO.setFirstOperand(operandDTO.getOperand());
        } else {
            // if so, set the second operand and calculate
            log.debug("Second operand set to "+operandDTO.getOperand()+". Beginning calculation");
            calculationDTO.setSecondOperand(operandDTO.getOperand());
            return calculateResult(calculationDTO);
        }
        return calculationDTO.getFirstOperand();
    }

    /**
     * Adds an operator to the calculation DTO. If the operator is a ROOTOF, calculation is started immediately, as no
     * second operand is required. The result of the calculation is then returned. In all other cases, no calculation takes
     * place yet, and the value of the first operand is returned instead.
     * @param operatorDTO DTO containing the operator value
     * @param calculationDTO DTO containing the calculation values
     * @return output to be presented to the user as a double
     */
    public double setOperator(OperatorDTO operatorDTO, CalculationDTO calculationDTO) throws InvalidRequestException {
        // validate input
        requestValidation.validateOperatorDTO(operatorDTO);
        calculationDTO.setOperator(operatorDTO.getOperator());
        if (operatorDTO.getOperator().equals("ROOTOF")) {
            log.debug("Operator set to ROOTOF, starting calculation");
            return calculateResult(calculationDTO);
        }
        log.debug("Operator set to "+calculationDTO.getOperator());
        return calculationDTO.getFirstOperand();
    }

    /**
     * Calculate the outcome of the values present in the calculationDTO and output the result
     * @param calculationDTO DTO containing the calculation values
     * @return the calculation result as a double
     * @throws ArithmeticException thrown in case errors in the calculation or an invalid operator
     */
    private double calculateResult(CalculationDTO calculationDTO) throws ArithmeticException {
        double firstOperand = calculationDTO.getFirstOperand();
        double secondOperand = calculationDTO.getSecondOperand();
        double result;
        switch(calculationDTO.getOperator()) {
            case "ADD":
                result=firstOperand+secondOperand;
                break;
            case "SUBTRACT":
                result=firstOperand-secondOperand;
                break;
            case "DIVIDE":
                result=firstOperand / secondOperand;
                break;
            case "MULTIPLY":
                result=firstOperand * secondOperand;
                break;
            case "ROOTOF":
                result=Math.sqrt(firstOperand);
                break;
            case "POWEROF":
                result=Math.pow(firstOperand, secondOperand);
                break;
            default:
                throw new ArithmeticException("Invalid operator. Allowed inputs are: ADD, SUBTRACT, DIVIDE, MULTIPLY, ROOTOF, POWEROF");
        }
        postCalc(calculationDTO, result);
        return result;
    }

    /**
     * Resets the CalculationDTO to be ready for further input after successful calculation. The calculated value is set as
     * the first operand, and the operator is again set to null. This allows the user to continue calculating with the resulting value
     * @param calculationDTO DTO containing the calculation values
     * @param calculationResult the calculation result
     */
    private void postCalc(CalculationDTO calculationDTO, double calculationResult) {
        log.debug("Calculation completed. Preparing for another round");
        calculationDTO.setFirstOperand(calculationResult);
        calculationDTO.setOperator(null);
    }
}
