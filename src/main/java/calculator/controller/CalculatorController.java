package calculator.controller;

import calculator.model.CalculationDTO;
import calculator.model.OperandDTO;
import calculator.model.OperatorDTO;
import calculator.service.CalculationService;
import calculator.validations.InvalidRequestException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Scope;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpSession;

@Slf4j
@RestController
@Scope("request")
@EnableAutoConfiguration
public class CalculatorController {

    @Autowired
    private CalculationService calculationService;
    @Autowired
    private CalculationDTO calculationDTO;

    /**
     * Adds an operand for calculation. If no operator is present yet, the first operand will be set. If an operator is
     * present, the second operand will be set and calculation will be performed.
     * @param session httpSession
     * @param operandDTO the operand with the desired value
     * @return the first operand, or a calculated result if input is the second operand in the calculation.
     */
    @PostMapping("/addOperand")
        double addOperand(HttpSession session, @RequestBody OperandDTO operandDTO) {
        try {
            log.debug("Adding operand for session: "+session.getId());
            return calculationService.addOperand(operandDTO, calculationDTO);
        }
        catch (ArithmeticException | InvalidRequestException exception) {
            // in case of an error, also clear the data so calculation can begin anew
            clear(session);
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    /**
     * Sets the operator for the calculation. If the operator is a Square Root (ROOTOF), calculation is performed immediately.
     * Otherwise, a second operand is required.
     * Allowed values are: ADD, SUBTRACT, DIVIDE, MULTIPLY, ROOTOF, POWEROF
     * @param session the httpSession
     * @param operatorDTO the operator with the desired value
     * @return the first operand, or a calculated result if the operator is ROOTOF
     */
    @PostMapping("/setOperator")
        double setOperator(HttpSession session, @RequestBody OperatorDTO operatorDTO) {
        try {
            log.debug("Setting operator for session: "+session.getId());
            return calculationService.setOperator(operatorDTO, calculationDTO);
        }
        catch (ArithmeticException | InvalidRequestException exception) {
            // in case of an error, also clear the data so calculation can begin anew
            clear(session);
            throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @DeleteMapping("/clear")
        void clear(HttpSession session) {
        session.setAttribute("calculationDTO", null);
    }

}
