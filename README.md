# ReadMe

## Install instructions

### Requirements
- Maven
- Redis server running on port 6379

### How to run
1. Clone project
2. Run from root folder using $ mvn spring-boot:run

### How to test
- Set the first Operand 
    - default path: http://localhost:8081/calculator/addOperand
    - body: { "Operand": _doubeValue_}
- Take the cookie SESSION value from this first request
- Set the Operator
    - default path: http://localhost:8081/calculator/setOperator
    - header: Name: Cookie | Value: sessionid=_cookieValueTakenFromPreviousRequest_
    - body: { "Operator": _"ADD"_} (or any other valid input)
- Set Second Operand
    - default path: http://localhost:8081/calculator/addOperand
    - header: Name: Cookie | Value: sessionid=_cookieValueTakenFromPreviousRequest_
    - body: { "Operand": _doubeValue_}

Swagger documentation is available on (default):
_http://localhost:8081/calculator/swagger-ui.html_

## Development Process

### Initial conditions

Basic calculator that can perform the following operations:
- Add
- Subtract
- Divide
- Multiply
- PowerTo
- RootOf

**Happy flow scenarios:**
1. User inputs first operand
2. User inputs operator
3. User inputs second operand
4. User selects 'equals'
5. System provides calculated result

**Sub scenario 1:**
1. User selects 'clear'
2. User starts a new calculation

**Sub scenario 2:**
1. User continues calculation on the previous result
2. User inputs new operator
3. User inputs new operand
4. User selects 'equals'
5. System provides calculated result

The system should maintain a session for the user so subsequent inputs can be combined. 

### Initial considerations 

**Solution 1:**
Always pass the entire calculation request in one go (operand, operator, operand), and return a calculated result.
Simple and straightforward, as much of the work is handled by the front-end. 
It would not require anything in the way of sessions, as any calculation request can be considered individually.
*Conclusion: Would probably be my first choice, but not compliant with assignment* 

**Solution 2:**
Pass every individual user input to the backend (each keystroke, each backspace, etc) and handle everything server-side.
Seems overly complex and traffic-heavy. 
*Conclusion: Impractical*

**Solution 3:**
Pass the operands and operators to the backend as separate inputs. Ensure the server handles these reliably based on different scenarios.
*Conclusion: Apparent best option*

It would appear the chosen scenario requires only three basic REST services: 
- POST an operand
- POST an operator
- DELETE clear all input and start over

In happy flow, this would result in:

1. User inputs first operand
2. User inputs operator
    3. Front-end POSTs first operand 
    4. Server returns value (first operand)
    5. Front-end displays returned value
6. User inputs second operand
    7. Front-end POSTs operator
    8. Server returns value (first operand)
    9. Front-end displays returned value
10. User selects 'equals'
    11. Front-end POSTs second operand
    12. Server calculates and returns value (calculated result)
    13. Front-end displays returned value

### Choices

- Considered working with BigDecimal for added precision, but BigDecimal is somewhat harder to work with in combination with
the .math libs. Opting for double instead. This should easily be sufficient for a basic calculator.
- Limited myself to calculating the square root of an input. Implementing full-fledged nth root calculation would probably
double the code base for the application. 
- Request-based controller with Session-scoped data-carrier. Disadvantage is that this creates a new controller with each
request, but given how small the controller is, this should be no problem. It is the easier and more readable solution.
- Actions such as divide-by-zero do not cause an exception in the case of doubles, but return 'infinity' (or NaN in other cases)
This is apparently done from a robustness standpoint, and because they are valid results in a number of mathematical scenarios.
I've considered trying to catch these in a generic ArithemticException and return a 400/Bad Request in those cases, but decided
against it. Depending on the setup of the front-end this may yet be changed.

### Not done

- I've struggled a fair bit getting a good unittest up and running for the CalculatorController. The fact that it is requestScoped
seems to cause no end of problems in the test. There's several bug-reports out there, but not a reliable solution that I've seen.
Given a few more hours I can probably resolve it, but at some point a man's got to eat...
- Most invalid inputs to the POST services are caught by the JSON parser, and give technically correct, but somewhat complicated
error messages. With a bit more time I would probably look at handling this a little more elegantly, with more user-friendly 
error responses.
- Swagger UI display is the default output and therefore a bit messy. For a public production application I'd probably 
spend some time finetuning it to something a bit more swanky.  
- Normally I would default to a Master<->Development<->Feature-Branch strategy for Git, with linear history/rebase. Given 
that I'm the only one working on the project, I've stuck with Master<->Development for ease sake in this case. 

### Time taken (approximately)

- [45 mins] Setting up local build tools, repo, redis etc
- [5 mins] Fixing the damned coffee machine in the office
- [20 mins] Set up base app and HelloWorld
- [30 mins] Write up initial design and expected services
- [30 mins] Set up Spring Security and Redis connection for sessions
- [60 mins] Implement CalculationService and Model classes 
- [60 mins] Attempt to create a full-blown nth-root calculation. Try several different options. Decide it's overly 
complicated for this test app. Gave up 
- [60 mins] Figure out why the Rest Controller was only partially persisting content of the CalculationDTO in the sesion
- [30 mins] Implement services
- [60 mins] Figure out why my SecurityConfig class was not correctly overwriting the default Spring security (hint: the config 
folder was one step to high in the project structure. D'oh)
- [30 mins] Write unittests
- [60 mins] Futile attempt to write a proper unittest for the controller. Getting late. Getting hungry. Almost smashed keyboard. Gave up.
- [10 mins] Added basic swagger documentation
- [30 mins] Writing this here sample of modern literature
- [10 mins] Slight refactor to Controller based on under-the-shower-insight


